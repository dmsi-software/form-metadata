const fs = require('fs');
const path = require('path');
const { promisify } = require('util');
const { exec_sync } = require('child_process');

const pkg = require('./package.json');

const read = async (p) => (await promisify(fs.readFile)(p)).toString();
const write = promisify(fs.writeFile);

const makeVersionBadge = (version) =>
  `[![version](https://img.shields.io/badge/metadata-v${version}-brightgreen)](#status)`;

!(async function () {
  const filename = path.join(__dirname, 'README.md');
  const readme = await read(filename);
  const regexpr = /\[!\[version](.*)\(#status\)/;

  const nextReadme = readme.replace(regexpr, makeVersionBadge(pkg.version));
  await write(filename, nextReadme);
  exec_sync('git add README.md');
})();
