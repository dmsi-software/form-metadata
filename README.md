# Metadata

[![version](https://img.shields.io/badge/metadata-v0.5.0-brightgreen)](#status)

## Status

The current approach to versioning is still under debate, specifically what is considered a version 1 (the current specification is pre-v1). The expectation is that the metadata will follow [Semantic Versioning](https://semver.org/spec/v2.0.0.html) and track changes to the spec in a manner that conforms to [Keep a Changelog](https://keepachangelog.com/en/1.0.0/). Keep in mind that new changes to the metadata specification **must always be backwards compatible**.

In addition to the actual version being in flux, the official name of this specification outside of "metadata" has neither been decided nor discussed.

## Introduction

Metadata is a JSON object that provides an abstract specification of UI forms both in their included elements and layout.

The metadata API is designed to abstract the development of CRUD forms away to a specification of its parts, leaving the layout to a rigidly enforced design system or other renderer. The metadata API will not enforce the method by which it is rendered.

## Conventions

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”, “SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in this document are to be interpreted as described in [RFC 2119](http://tools.ietf.org/html/rfc2119).

## Document Structure

This section describes the content of a metadata document. Metadata documents are defined in [JavaScript Object Notation (JSON)](http://tools.ietf.org/html/rfc7159).

Unless otherwise noted, objects defined by this specification **MUST NOT** contain any additional members. Client implementations **MUST** ignore members not recognized by this specification.

### Top Level

A JSON object **MUST** be at the root of every metadata document. This object defines a document’s “top level”.

A document **MUST** contain at least the following top-level members:

- `rows`: An array of [rows](#rows) that serve as the sheet definition for the document.
- `fields`: An object of [fields](#fields) keyed by the `fieldId`.
- `layouts`: An array of [sheets](#sheets) that each act as full sheets.

`layouts` **SHOULD** be an array, however in cases where there is only one sheet defined in the document, it **MAY** contain only a single [sheet object](#sheets). This will need to be accounted for in the render engine that consumes the document.

A document **MAY** contain any of the following top-level members:

- `actions`: An [action object](#actions) that contains information to render the submit and any additional form buttons.
- `meta`: A [meta object](#meta) object that contains non-standard meta-information
- `template`: A forthcoming definition designed to inform the renderer how to render the document.

The definition of the `template` member will change, as such, metadata **MAY** place template information under the `meta` object for the time-being.

### Rows

"Rows" appear in metadata documents to communicate the anticipated sheet of a single row in the rendered UI.

A row **MUST** contain at least the following top-level members:

- `columns`: An array of field identifiers or '.'.
- `id`: A unique identifier for the row.

A row **MAY** contain the following top-level member:

- `indent`: A number indicated the number of units from the left the row should be indented. For example, a value of `1` would be the equivalent of one tab stop in a standard document. Defaults to `0`.

### Sheets

"Sheets" appear in metadata documents to collect and align rows into macro-columns. A metadata document can contain one or many sheets which can be rendered in sequence. The behavior of displaying sheets **SHOULD** be detailed in the `template` definition or handled directly by the render engine.

A sheet **MUST** contain at least the following top-level members:

- `columns`: A two dimensional array where each array represents a macro-column, and each item in the array is a row identifier.
- `id`: A unique identifier for the sheet.

A sheet **MAY** contain any of the following top-level members:

- `order`: A number greater than or equal to `0` indicating the sheet's order within the overall structure. This may be valuable if you are creating a wizard or tabbed form.

### Fields

"Fields" are the bread and butter of the metadata specification. Each document relies on fields to render forms. Fields are assigned to rows, which determines their position within the overall sheet.

Note that fields are keyed by a unique identifier. This will be used in `rows.columns` to specify sheet.

A field with a `type` of `layout.header` **SHOULD** be present in each sheet.

A field **MUST** contain at least the following top-level members:

- `label`: A string label describing the purpose of the field. The field label **MUST** be sentence-cased.
- `type`: A [field type](#field-types) identifier.

A field **SHOULD** contain the following top-level members. Each of these are **REQUIRED** when the `field.type` is prefixed with `form`, however, are **OPTIONAL** for most other field types.

- `name`: A string name describing where the item should live in the form state object.
- `internal`: An [internal settings object](#internal-settings-object)
- `disabled`: Whether the field should be set as disabled by default. This can be either a `boolean` or an [MQL](#mql) statement.
- `readOnly`: Whether the field should be set as read-only by default. This can be either a `boolean` or an [MQL](#mql) statement.
- `required`: Whether the field should be set as required by default. This can be either a `boolean` or an [MQL](#mql) statement.
- `skip`: Whether the field should be skipped in the tab order by default. This can be either a `boolean` or an [MQL](#mql) statement.
- `hidden`: Whether the field should be displayed or not. This can be either a `boolean` or an [MQL](#mql) statement.
- `tabIndex`: A number greater than `0` that can be used to directly control the order in which tabbing is nagivated through a form.

A field **MAY** contain any of the following top-level members:

- `context`: A string denoting fields that have the same provider. Currently only used in [date-picker](#date-picker) fields.
- `props`: An object whose member can be used to include non-standard component properties/attributes. These are key-value pairs where the key is the attribute/prop. Exactly what members are required respective to the `field.type` is dictated by the component library being used.
- `options`: An array of option objects whose members are `label` and `value`. These are designed to be used for selects, multiselects, radio groups, and checkbox groups to provide built in options. These are overridden by `externalOptions` matching the `name` of the field.
- `fieldSource`: A [field source object](#field-source-object).
- `info`: An [info object](#info-object) specifying an information tooltip.

#### Field Types

The following fields are recognized metadata fields. The fields listed in the HTML Tag are the functional equivalent of the field definition in semantic HTML and **MAY** be used to render the fields. However, any of the fields listed **MAY** be exchanged for their functional equivalent in the design system or component library of the renderer. Use of semantic HTML is **NOT REQUIRED**.

| ID                  | HTML Tag                                     | Description                                                                                    |
| ------------------- | -------------------------------------------- | ---------------------------------------------------------------------------------------------- |
| `form.input`        | `<input />`                                  |                                                                                                |
| `form.textarea`     | `<textarea></textarea>`                      |                                                                                                |
| `form.select`       | `<select></select>`                          |                                                                                                |
| `form.multi-select` | `<select multiple></select>`                 |                                                                                                |
| `form.radio-group`  | `<input type="radio" />`                     |                                                                                                |
| `form.date-time`    | `<input type="date" /><input type="time" />` | A date and time input side by side which output an ISO standard date string                    |
| `form.date`         | `<input type="date" />`                      | A date and time input side by side which output a date string with format 'YYYY/MM/DD'         |
| `form.time`         | `<input type="time" />`                      | A date and time input side by side which output a time string with format 'hh:mm'              |
| `form.checkbox`     | `<input type="checkbox" />`                  |                                                                                                |
| `form.time`         | `<input type="time" />`                      |                                                                                                |
| `form.number`       | `<input type="number" />`                    |                                                                                                |
| `form.currency`     | `<input type="number" step=".01" />`         |                                                                                                |
| `form.percent`      | `<input type="number" max="100" />`          |                                                                                                |
| `form.password`     | `<input type={"text" or "password"} />`      |                                                                                                |
| `form.email`        | `<input type="text" />`                      |                                                                                                |
| `form.switch-group` | Group of `<input type="checkbox" />`         |                                                                                                |
| `form.mask`         | `<input type="text" />`                      | A standard text input that receives patterns, displays masked text, and outputs unmasked text. |
| `layout.header`     | `<h2></h2>`                                  |                                                                                                |
| `layout.subheader`  | `<h3></h3>`                                  |                                                                                                |
| `advanced.xref`     | N/A                                          |                                                                                                |
| `advanced.locator`  | N/A                                          |                                                                                                |
| `action.button`     | `<button type="button" />`                   | A standard button element.                                                                     |
| `action.icon`       | `<button type="button" />`                   | A styled, circular HTML button containing only an icon (no text). element.                     |

#### Internal Settings Object

"Internal Settings" are settings meant to lock down the four [customizable](#customization) elements of a field. Their corresponding properties on the top-level of the `field` determine how the field _actually_ behaves, while the internal settings indicate what may be changed by the user.

Internal settings **MUST** contain the following top-level members:

- `editable`: A boolean denoting whether a user may edit the label of the field.
- `skippable`: A boolean denoting whether a user may remove the field from the tab order.
- `removable`: A boolean denoting whether a user can remove the field from the form.
- `disableable`: A boolean denoting whether a user can disable/make readOnly the field.

#### Field Source Object

A "field source" is a specification for both the settings on a `field` as well as an implementation of rendering components. A "field source" allows data, picked from a source external to the form but defined in the form, to be mapped onto fields defined in the metadata document. See [rendering field sources](#rendering-field-sources) for more information on the impact a field source has on the form.

A field source **MUST** contain at least the following top-level members:

- `name`: The name of the `field` from which the data should be sourced.
- `location`: The dot path to the value source of the `field`.
  Say the data provided by the source is:
  ```json
  {
    "data": {
      "id": "123",
      "type": "sales-order",
      "attributes": {
        "customerName": {
          "first": "Billy",
          "last": "the Kid"
        }
      }
    }
  }
  ```
  And say the current field was to display the first name of a customer. In that case, you would provide `location` with the value: `data.attributes.customerName.first`. So when the source resolved, it would fill the field with "Billy".

A field source **SHOULD** contain the following top-level members:

- `type`: This is a generic string which the render engine **SHOULD** have the appropriate solution for. This **SHOULD** be included, however if the render engine only recognizes one type of field source, this **MAY** be ommitted. Please note that there is an inherent danger to making this decision rashly, as the introduction of different field source types will need to be accounted for, including the pre-existing omissions.

### Info Object

The info object specifies an informational tooltip that can be added to a field's label.

The info object **MUST** have the following top-level members:

- `title`: The title of the informational tooltip.

The info object **MAY** have the following top-level members:

- `content`: The text to display in the tooltip.
- `link`: a link object specifying a link to add to the bottom of the tooltip.

If the `link` property is present, it **MUST** have the following sub-properties:

- `url`: The url the link should redirect to.
- `label`: The link's display text.

The info object takes on the following form:

```json
{
  "info": {
    "title": "Informational Tooltip",
    "content": "This is the tooltip description.",
    "link": {
      "url": "https://www.dmsi.com/",
      "label": "Learn More!"
    }
  }
}
```

### actions

When specified, the actions object can be used to customize the form action buttons for the needs of the application. The actions object **MAY** have the following top-level members:

- `submit`: [submit object](#submit)
- `top`: array of [action item identifiers](<#action-rows-(top/bottom)>)
- `bottom`: array of [action item identifiers](<#action-rows-(top/bottom)>)

#### Action Rows (`top`/`bottom`)

When specified, `top` and `bottom` indicate that an Action Row should be rendered at the top and/or bottom of the form. A row is specified by an array of action item identifiers. And action item identifier is one of the following:

- '@submit' - Submit button
- '.' - Spacer
- \<string\> - Name of an `action.button`

If one or more action rows are specified, at least one **MUST** contain a `@submit`.

#### submit

When specified, the submit object can customize the submit button on the form. Regardless of changes made, the submit button will always call the `onSubmit` callback when pressed. The submit object **MAY** have the following top-level members:

- `domain`: 'primary' | 'success' | 'warning' | 'danger' | 'default' | 'white'
- `label`: A string which customizes the submit button label. _Default: "Submit"_
- `icon`: Indicates that the submit button should be an icon. If defined, it must be a string which is a valid icon name in `@wedgekit/icons`
- `iconLabel`: A boolean which indicates whether the label should appear to the right of the icon. _Only has an effect if `icon` is specified._

#### action.button

Specified in the top-level `fields` property, `action.button` field types may be customized in a similar manner to the `submit` button. An `action.button` requires a callback to be present in `actionCallbacks` that will be fired when the button is pressed. The `action.button` object **MAY** have the following top-level members:

- `label`: A string which customizes the button label. The `action.button` label **MUST** be title-cased.

And **MAY** have the following `props` members:

- `domain`: 'primary' | 'success' | 'warning' | 'danger' | 'default' | 'white'
- `icon`: Indicates that the button should contain an icon. If defined, it must be a string which is a valid icon name in `@wedgekit/icons`.
- `variant`: 'neutral' | 'noFill' | 'outline'

#### action.icon

Specified in the top-level `fields` property, `action.icon` field types display a circular HTML button. The `action.icon` **MUST** contain an icon defined in `@wedgekit/icons`. The `action.icon` object **MAY** have the following top-level members:

- `label`: A string which customizes the button label. The `action.icon` label is meant to be displayed as a tooltip - whether or not the tooltip is displayed can be controlled with the 'tooltip' prop.

And **MAY** have the following `props` members:

- `noFill`: Creates the `action.icon` without a background-color property.
- `tooltip`: Indicates that the label should display as a tooltip over the `action.icon` when hovered.

### Meta

Where specified, a `meta` member can be used to include non-standard meta-information. The value of each `meta` member **MUST** be an object (a “meta object”).

Any members **MAY** be specified within meta objects.

### Template

Template relays information about the design layout to the renderer. In order to keep this object tight, only information **REQUIRED** by the renderer should be included as a top-level member.

A template **MAY** contain the following top-level members:

- `toc`: A boolean denoting whether or not the renderer should render a table of contents made up of the `layout.subheader` fields and `layout.header` fields.

## Rendering

### Table of Contents

A table of contents **SHOULD NOT** break during the rendering process when there are few or no `layout.subheader` fields or `layout.header` fields present.

## Customization

## MQL

Each of the `field` state properties (`skip`, `disabled`, `required`, `readOnly`, `hidden`) **MAY** use an internal query syntax in favor of a boolean to denote dynamic state changes. The query syntax is called MQL, and is designed to aid in readability for a non-developer user, while avoiding a subset of an existing language.

MQL makes no assumption about the shape of the AST resulting from a parsed statement nor how an AST (or similar) is turned into an MQL statement. The parser and encoder **MUST** conform to each of the specifications below and support each of the [operators](#operators) specified. Parsers **SHOULD NOT** add to the list of operators nor add additional features like arithmetic or functions.

The left side of each argument **MUST** be the `field.name` of another field defined in the metadata document.

The right side of the argument, when the [operator](#operators) requires it, **MUST** be a string that is not enclosed by any encapsulation characters. The inclusion of any encapsulation characters, escaped or otherwise, will be considered part of the evaluated string.

When implementing a parser both to encode and decode MQL, `GREATER_THAN`, `GREATER_THAN_OR_EQUALS`, `LESS_THAN`, and `LESS_THAN_OR_EQUALS` **SHOULD** have both sides converted to numbers before comparing them, only falling back to strings if both sides cannot be converted. This will prevent scenarios like `'9' < '12' // => false` from occurring. Otherwise, values are evaluated as strings, as HTML values tend to be either strings or booleans.

An MQL statement **MAY** include the following characters for creating compound conditions:

- `&&`: Read as "and." Creates a conjunction where the statement to the left and right of symbol must evaluate to `true` in order to pass.
- `||`: Read as "or." Creates a disjunction where either the statement to the left or the statement to the right must evaluate to `true` in order to pass, both both need not.

Parentheses (`()`) may be employed to properly encapsulate conditions. These statements **MUST** be evaluated first in order to properly compare with other statements.

### Operators

Each of the operators **MUST** be available. The field types column contain each of the field types that a given operator **MAY** be associated with. Note that the field type here refers to the type of the field on the left side of the statement. That is, in the case of: `customerID EQUALS 12`, `customerID` is the field to which the type refers.

| Name                     | Description                                                                                                                                                                     | Syntax                                                                         | Parsed (JS)                              | Field Types                                                                                                                    |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------ | ---------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------ |
| `EQUALS`                 | Whether the value of the `field` on the left matches the value on the right.                                                                                                    | `<field.name> EQUALS 22`                                                       | `<field.name> === '22'`                  | All `form` fields except `form.multi-select`; `form.switch-group`                                                              |
| `NOT_EQUALS`             | Whether the value of the `field` on the left does not match the value on the right.                                                                                             | `<field.name> NOT_EQUALS 22`                                                   | `<field.name> !== '22'`                  | All `form` fields except `form.multi-select`; `form.switch-group`                                                              |
| `TRUTHY`                 | Whether the value of the `field` on the left is truthy.                                                                                                                         | `<field.name> TRUTHY`                                                          | `!!<field.name>`                         | All `form` fields except `form.number`; `form.percent`; `form.currency`                                                        |
| `FALSY`                  | Whether the value of the `field` on the left is falsy.                                                                                                                          | `<field.name> FALSY`                                                           | `!<field.name>`                          | All `form` fields except `form.number`; `form.percent`; `form.currency`                                                        |
| `GREATER_THAN`           | Whether the value of the `field` on the left is greater than the value on the right.                                                                                            | `<field.name> GREATER_THAN 10`                                                 | `<field.name> > 10`                      | A `form.input` serving as a number; `form.date`; `form.date-time`; `form.time`; `form.number`; `form.percent`; `form.currency` |
| `GREATER_THAN_OR_EQUALS` | Whether the value of the `field` on the left is greater than or equal to the value on the right.                                                                                | `<field.name> GREATER_THAN_OR_EQUALS 10`                                       | `<field.name> >= 10`                     | A `form.input` serving as a number; `form.date`; `form.date-time`; `form.time`; `form.number`; `form.percent`; `form.currency` |
| `LESS_THAN`              | Whether the value of the `field` on the left is less than the value on the right.                                                                                               | `<field.name> LESS_THAN 10`                                                    | `<field.name> < 10`                      | A `form.input` serving as a number; `form.date`; `form.date-time`; `form.time`; `form.number`; `form.percent`; `form.currency` |
| `LESS_THAN_OR_EQUALS`    | Whether the value of the `field` on the left is less than or equal to the value on the right.                                                                                   | `<field.name> LESS_THAN_OR_EQUALS 10`                                          | `<field.name> <= 10`                     | A `form.input` serving as a number; `form.date`; `form.date-time`; `form.time`; `form.number`; `form.percent`; `form.currency` |
| `BEFORE`                 | An alias for `LESS_THAN`. It is primarily designed to respond to date inputs where the value would be an ISO-8601 date string, thus allowing for a normal string comparison.    | `<field.name> BEFORE 2020-01-01T09:30:00Z`                                     | `<field.name> < '2020-01-01T09:30:00Z'`  | `form.date`; `form.date-time`; `form.time`                                                                                     |
| `AFTER`                  | An alias for `GREATER_THAN`. It is primarily designed to respond to date inputs where the value would be an ISO-8601 date string, thus allowing for a normal string comparison. | `<field.name> AFTER 2020-01-01T09:30:00Z`                                      | `<field.name> > '2020-01-01T09:30:00Z'`  | `form.date`; `form.date-time`; `form.time`                                                                                     |
| `BETWEEN`                | Whether the value of the `field` on the left is between the first and second values on the right.                                                                               | `<field.name> BETWEEN 12 34`                                                   | `<field.name> > 12 && <field.name> < 34` | A `form.input` serving as a number; `form.date`; `form.date-time`; `form.time`; `form.number`; `form.percent`; `form.currency` |
| `CONTAINS`               | Whether the value of the `field` on the left contains the value on the right.                                                                                                   | `<field.name> CONTAINS kittens`                                                | `<field.name>.includes('kittens')`       | A `form.input` serving as a text; `form.textarea`; `form.switch-group`; `form.mask`                                            |
| `STARTS_WITH`            | Whether the value of the `field` on the left starts with the value on the right.                                                                                                | `<field.name> STARTS_WITH kit`                                                 | `<field.name>.startsWith('kit')`         | A `form.input` serving as a text; `form.textarea`; `form.mask`                                                                 |
| `ENDS_WITH`              | Whether the value of the `field` on the left ends with the value on the right.                                                                                                  | `<field.name> ENDS_WITH ten`                                                   | `<field.name>.endsWith('ten')`           | A `form.input` serving as a text; `form.textarea`; `form.mask`                                                                 |
| `SET_VALUE`              | When the MQL expression on the left evaluates to true, sets the value of the `field` to the value or expression on the right.                                                   | `<field.name> TRUTHY SET_VALUE 100`                                            | `<field.value> = 100`                    | All `form` fields except `form.multi-select`; `form.switch-group`                                                              |
| `THEN`                   | When the `SET_VALUE` expression on the left evaluates to true, returns the value on the right to update the `field`.                                                            | `...SET_VALUE <field.name> TRUTHY THEN 100`                                    |                                          | All `form` fields except `form.multi-select`; `form.switch-group`                                                              |
| `ELSE`                   | Separator for multiple `SET_VALUE` expressions. If the expression on the left does not evaluate to true, the expression on the right is evaluated.                              | `...SET_VALUE <field.name> TRUTHY THEN 100 ELSE <field2.name> TRUTHY THEN 200` |                                          | All `form` fields except `form.multi-select`; `form.switch-group`                                                              |

When implementing a parser both to encode and decode MQL, `GREATER_THAN`, `GREATER_THAN_OR_EQUALS`, `LESS_THAN`, and `LESS_THAN_OR_EQUALS` **SHOULD** have both sides converted to numbers before comparing them, only falling back to strings if both sides cannot be converted. This will prevent scenarios like `'9' < '12' // => false` from occurring.

### Form and Field State

By default, MQL will evaluate the value of a field. However, it can also be configured to evaluate other aspects of the [form and field state](https://bitbucket.org/dmsi-software/wedgekit/src/develop/packages/render/FORM_STATE.md).

To evaluate field state, MQL should be configured as `<field-name>$<field-state> <operator>`

Example:

```json5
{
  disabled: 'name$dirty TRUTHY',
  required: 'accomplices$length GREATER_THAN 2',
}
```

To evaluate form state, MQL should be configured as `@<form-state> <operator>`

Example:

```json5
{
  disabled: '@touched FALSY',
}
```

### SET_VALUE, THEN, ELSE Operators

The `SET_VALUE` operator **SHALL** only appear within one of the `field` state properties (`disabled`, `hidden`) and **MUST** be preceeded by a valid MQL expression.

The `THEN` and `ELSE` operators **SHALL** only appear within the context of a `SET_VALUE` expression.

When a `field` state property evaluates to true, the `SET_VALUE` expression is evaluated. If a valid value is returned, the `value` of the `field` is set to the returned value.

`THEN` values **MAY** be a string value or one of `TRUE`, `FALSE`, or `NULL`. The latter three values will be set to their corresponding javascript primitives, rather than a string.

### Example

The following JS conditional statement:

```js
const isRequired = !!poRequired.value;
```

would roughly equate to the following MQL statement:

```json
{
  "name": "qualityCheck",
  "type": "form.checkbox",
  "required": "poRequired TRUTHY"
}
```

## Glossary

### Terms

| Name         | Locations                             | Description |
| ------------ | ------------------------------------- | ----------- |
| column       | `rows.columns`<br />`layouts.columns` |             |
| row          | `rows`                                |             |
| macro column | `layouts.columns`                     |             |
| sheet        | `sheets`                              |             |
| field        | `fields`                              |             |
| component    | `fields`                              |             |

### Sheet Example

[![Glossary fig 1](https://i.imgur.com/SdQhtIe.png)](#glossary)

In the figure above, the blue outline is a single [`sheet`](#sheets).

The red outline is a "macro column" located in `layouts.columns`. This is a collection of rows. The first row in this case is outlined in orange and would be defined in `rows`.

Further, in the case of this outlined row, there are 3 columns, one for each of the text inputs and one for the locator button. These would be defined in `rows.columns` and each field defined in `fields`.
