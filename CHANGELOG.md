# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

---

## [Unreleased]

### Added

- [JUN-383] - Added field and form state MQL evaluations
- [JUN-194] - Added `layout.header` to the component types.
- [JUN-653] - Added definition for `action.icon`.

### Changed

- [JUN-190] changed layout.section to layout.subheader
- [JUN-191] Changed definition of layouts.
- [JUN-653] Changed definition for `action.button`.

### Deprecated

### Removed

### Fixed

### Security

---

## [0.5.0]

### Added

- Added `form.number`, `form.currency`, and `form.percent` to the component types.
- [JUN-231] - Added `form.switch-group` to the component types.
- [JUN-143] - Added `form.mask` to the component types.
- [JUN-224] - Added action rows (`top`, `bottom`) to `actions`

### Changed

- [JUN-267]:[BREAKING] - `fields` is now an object keyed by the `fieldId`.

---

## [0.4.5] 2020-10-20

### Added

- [JUN-215] - Added submit button options to metadata spec.

---

## [0.4.4] 2020-10-09

### Added

- [JUN-215] - Added submit button options to metadata spec.

---
